import 'dart:async';
// needed for custom tabs accent color
import 'package:flutter/material.dart';

import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';

Future<void> launchUri(Uri url) async {
  try {
    await launch(
      url.toString(),
      option: new CustomTabsOption(
//        toolbarColor: Theme.of(context).primaryColor,
        toolbarColor: Colors.blueGrey,
        enableDefaultShare: false,
        enableUrlBarHiding: true,
        showPageTitle: false,
      ),
    );
  } catch (e) {
    // An exception is thrown if browser app is not installed on Android device.
    debugPrint(e.toString());
  }
}
