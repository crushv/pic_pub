import 'dart:async';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:http/http.dart' as http;

import 'package:pic_pub/api/api.dart';

class MastoApi extends Api {
  MastoApi(Uri server, oauth2.Client client) : super(server, client);

  // TODO: add stuff to handle 403s
  Future<void> startSession() async {
    http.Response resp =
    await super.client.get(super.getEndpoint('/api/v1/accounts/verify_credentials'));
    print(resp.body);
  }

}