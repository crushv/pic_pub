import 'package:oauth2/oauth2.dart' as oauth2;

class Api {
  Uri _serverUri;
  oauth2.Client _client;

  Uri get serverUri => _serverUri;
  oauth2.Client get client => _client;

  Api(Uri server, oauth2.Client client) {
    this._serverUri = server;
    this._client = client;
  }

  Uri getEndpoint(String path) {
    return new Uri(
        scheme: this.serverUri.scheme,
        host: this.serverUri.host,
        port: this.serverUri.port,
        path: path);
  }

  static bool statusValid(int code) {
    return code >= 200 && code < 300;
  }
}

class CommError implements Exception {
  CommError();
}