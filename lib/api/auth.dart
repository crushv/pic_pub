import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:oauth2/oauth2.dart' as oauth2;
import 'package:http/http.dart' as http;
import 'package:uni_links/uni_links.dart';

import 'package:pic_pub/api/api.dart';
import 'package:pic_pub/util/custom_tabs.dart';

/*
storage:

'credentials': String: credentials json
'client_id': String
'client_secret': String

'server': Uri.toString
 */

class MastoAuth {
  Uri _serverUri;
  Uri get serverUri => _serverUri;
  Uri _authorizationEndpoint;
  Uri _tokenEndpoint;
  SharedPreferences _prefs;

  final Uri _redirectUri = Uri.parse('picpub://oauth');

  // can't do async constructor so the async return of the client must be done in a separate func
  MastoAuth(String server) {
    Uri givenUri = Uri.parse(server);

    if (!givenUri.hasAuthority) {
      // assume https
      this._serverUri =
      new Uri(scheme: 'https', host: server);
    } else {
      this._serverUri = givenUri;
    }
    this._authorizationEndpoint = _getEndpoint('/oauth/authorize');
    this._tokenEndpoint = _getEndpoint('/oauth/token');
  }

  Future<bool> apiAvailable() async {
    if (this._prefs == null)
      this._prefs = await SharedPreferences.getInstance();

    try {
      http.Response resp = await http.get(_getEndpoint('/api/v1/instance'));
      if (resp.statusCode != 200) return false;
    } on http.ClientException catch (e) {
      print('Unable to detect mastoapi: $e');
      return false;
    } catch (e) {
      print('Unkown exception: $e');
      return false;
    }
    this._prefs.setString('server', this._serverUri.toString());
    return true;
  }

  Uri _getEndpoint(String path) {
    return new Uri(
        scheme: this._serverUri.scheme,
        host: this._serverUri.host,
        port: this._serverUri.port,
        path: path);
  }

  void _storeCreds(oauth2.Credentials credentials) {
    this._prefs.setString('credentials', credentials.toJson());
  }

  Future<oauth2.Client> getClient() async {
    if (this._prefs == null)
      this._prefs = await SharedPreferences.getInstance();

    String identifier = this._prefs.getString('client_id');
    String secret = this._prefs.getString('client_secret');
    if (identifier == null || secret == null) {
      http.Response response = await http.post(this._getEndpoint('/api/v1/apps'), body: {
        'client_name': 'picpub',
        'redirect_uris': 'picpub://oauth',
        'scopes': 'read write follow'
      });
      if (response.statusCode != 200) throw new CommError();
      var result = json.decode(response.body);
      identifier = result['client_id'];
      secret = result['client_secret'];
      print(identifier);
      print(secret);
      this._prefs.setString('client_id', identifier);
      this._prefs.setString('client_secret', secret);
    }

    String credstore = _prefs.getString('credentials');
    if (credstore != null) {
      print('credstore not empty!');
      oauth2.Credentials credentials =
          new oauth2.Credentials.fromJson(credstore);
      return new oauth2.Client(credentials,
          identifier: identifier, secret: secret, updateCall: _storeCreds);
    }
    print('credstore empty!');

    oauth2.AuthorizationCodeGrant grant = new oauth2.AuthorizationCodeGrant(
        identifier, _authorizationEndpoint, _tokenEndpoint,
        secret: secret);

    print('launching oauth flow');
    await launchUri(grant.getAuthorizationUrl(this._redirectUri,
        scopes: ['read', 'write', 'follow']));

    await for (String link in getLinksStream()) {
      print(link);
      print('oauth flow done');
      Uri authuri = Uri.parse(link);

      return await grant.handleAuthorizationResponse(authuri.queryParameters,
          updateCall: _storeCreds);
    }
  }
}
