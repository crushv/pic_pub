import 'package:flutter/material.dart';

import 'package:pic_pub/pages/feed.dart';
import 'package:pic_pub/pages/search.dart';
import 'package:pic_pub/pages/createpost.dart';
import 'package:pic_pub/pages/alerts.dart';
import 'package:pic_pub/pages/profile.dart';

import 'package:pic_pub/api/masto.dart';

class RootPage extends StatefulWidget {
  RootPage(MastoApi api, {Key key}) : super(key: key);

  @override
  _RootPageState createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _currentIndex = 0;

  void _setIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        children: <Widget>[
          new Offstage(
            offstage: _currentIndex != 0,
            child: new TickerMode(
              enabled: _currentIndex == 0,
              child: new PicFeed(),
            ),
          ),
          new Offstage(
            offstage: _currentIndex != 1,
            child: new TickerMode(
              enabled: _currentIndex == 1,
              child: new PicSearch(),
            ),
          )
        ],
      ),
      // TODO: replace the entire ui (it is trash)
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _currentIndex,
        items: <BottomNavigationBarItem>[
          new BottomNavigationBarItem(
              icon: new Icon(Icons.home), title: new Text("Home")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.search), title: new Text("Search")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.add_box), title: new Text("Post")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.notifications), title: new Text("Alerts")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.person), title: new Text("You")),
        ],
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          _setIndex(index);
        },
      ),
    );
  }
}
