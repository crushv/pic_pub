import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:pic_pub/widgets/post.dart';

// NOTE: builders and images dont like each other

class PicFeed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return new PicPost(post: 'https://letsalllovela.in/media/392f09f8-7575-4e06-b4a2-0c7ce7bbe400/image.png');
      },
    );
  }
}

//class PicFeed extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return new ListView(
//      children: <Widget>[
//        new PicPost(post: 'https://letsalllovela.in/media/392f09f8-7575-4e06-b4a2-0c7ce7bbe400/image.png'),
//        new PicPost(post: 'https://letsalllovela.in/media/392f09f8-7575-4e06-b4a2-0c7ce7bbe400/image.png'),
//      ],
//    );
//  }
//}
