import 'package:flutter/material.dart';

import 'package:oauth2/oauth2.dart' as oauth2;

import 'package:pic_pub/pages/root.dart';

import 'package:pic_pub/api/api.dart';
import 'package:pic_pub/api/auth.dart';
import 'package:pic_pub/api/masto.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  void _logIn() async {
    if (_inputController.text.length == 0) return;
    print('Starting auth flow');
    try {
      setState(() {
        _buttonEnabled = false;
      });
      MastoAuth auth = new MastoAuth(_inputController.text);
      bool avail = await auth.apiAvailable();
      if (avail) {
        oauth2.Client client = await auth.getClient();
        MastoApi api = new MastoApi(auth.serverUri, client);
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (BuildContext context) {
          return new WillPopScope(child: new RootPage(api), onWillPop: () async => false);
        }));
      } else {
        displayDialog('Error connecting',
            'Please check that the URL has been entered correctly and that the domain is not blocked by your ISP.');
        setState(() {
          _buttonEnabled = true;
        });
      }
    } on CommError {

      displayDialog('Error connecting',
          'Please check that the URL has been entered correctly and that the domain is not blocked by your ISP.');
      setState(() {
        _buttonEnabled = true;
      });
    } catch (e) {
      // display modal?
      print('Unkown exception: $e');
      displayDialog('Error', 'An unknown exception has occured.');
      setState(() {
        _buttonEnabled = true;
      });
    }
  }

  bool _buttonEnabled = true;
  final _inputController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _inputController.dispose();
    super.dispose();
  }

  Future<void> displayDialog(String title, String message) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(child: Text(message)),
          actions: <Widget>[
            FlatButton(
              child: Text('Continue'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        padding: new EdgeInsets.symmetric(horizontal: 80.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
                child: new Image.asset('assets/logo.png'),
                padding: const EdgeInsets.symmetric(horizontal: 40.0)),
            new TextField(
              autofocus: true,
              keyboardType: TextInputType.url,
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                labelText: 'Instance',
              ),
              controller: _inputController,
            ),
            new SizedBox(
                width: double.infinity,
                child: new RaisedButton(
                  onPressed: _buttonEnabled ? _logIn : null,
                  color: Theme.of(context).accentColor,
                  child: const Text(
                    'CONNECT',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
