import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PicPost extends StatefulWidget {
  PicPost({Key key, this.post}) : super(key: key);

  // TODO: change this to a post class, not just an image url
  final String post;
  final String pfp =
      'https://letsalllovela.in/media/e27830d9-8157-430a-94ff-0c961ba914ec/096613ebf85a4fb007b01cb095d6257d8d8ce2c0b583da23b178bcf164921365.png';

  @override
  _PicPostState createState() => new _PicPostState();
}

// TODO: max height of image == width of screen
// TODO: edge of action icons and edge of text container doesnt line up
class _PicPostState extends State<PicPost> {
  @override
  Widget build(BuildContext context) {
    return new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            // (maybe change) the height is hardcoded to the height of the IconButton
            height: 48.0,
            child: new Row(
              children: <Widget>[
                new Expanded(
                    child: new Row(
                  children: <Widget>[
                    new Container(
                      margin: const EdgeInsets.only(left: 8.0, right: 6.0),
                      child: new CircleAvatar(
                        backgroundImage: CachedNetworkImageProvider(widget.pfp),
                        // TODO: make this shrink better FIXME: THESE PIXELS ARENT LOGICAL LIKE EVERYTHING ELSE
                        radius: 16.0,
                      ),
                    ),
                    new Text('@crushv@niu.moe',
                        style: new TextStyle(fontWeight: FontWeight.bold))
                  ],
                )),
                new IconButton(icon: new Icon(Icons.more_vert), onPressed: null)
              ],
            ),
          ),
//          new Image.network(widget.post),
          new CachedNetworkImage(imageUrl: widget.post),
          new Row(
            children: <Widget>[
              new IconButton(
                  icon: new Icon(Icons.favorite_border),
                  color: Colors.black,
                  onPressed: () {}),
              new IconButton(
                  icon: new Icon(Icons.comment),
                  color: Colors.black,
                  onPressed: () {}),
              new IconButton(
                  icon: new Icon(Icons.repeat),
                  color: Colors.black,
                  onPressed: () {}),
            ],
          ),
          new Container(
            // TODO: change to container(likes),container(column([comments])),container(timestamp)
              margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
              child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(margin: EdgeInsets.only(bottom: 8.0),child: new RichText(
                      text: new TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            new TextSpan(text: 'Liked by '),
                            new TextSpan(
                                text: '93 people',
                                style:
                                    new TextStyle(fontWeight: FontWeight.bold)),
                          ]),
                    )),
                    new RichText(
                      text: new TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            new TextSpan(
                                text: 'nik ',
                                style:
                                    new TextStyle(fontWeight: FontWeight.bold)),
                            new TextSpan(
                                text:
                                    'holy shit this took me a while and YES I KNOW ALIGNMENT IS OFF I\'LL FIX IT askskd alskdjlkasjdk asdkl jalksdj kl'),
                          ]),
                    ),
                  ]))
        ]);
  }
}
