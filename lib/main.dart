import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:oauth2/oauth2.dart' as oauth2;

import 'package:pic_pub/api/api.dart';
import 'package:pic_pub/api/auth.dart';
import 'package:pic_pub/api/masto.dart';

import 'package:pic_pub/pages/root.dart';
import 'package:pic_pub/pages/login.dart';

void main() => runApp(new PicPub());

class PicPub extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'PicPub',
      theme: new ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: new HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  SharedPreferences _prefs;

  void _login() async {
    if (this._prefs == null)
      this._prefs = await SharedPreferences.getInstance();
    String server = this._prefs.getString('server');
    if (server == null) {
      Navigator.of(context)
          .push(new MaterialPageRoute(builder: (BuildContext context) {
        return new WillPopScope(
            child: new LoginPage(), onWillPop: () async => false);
      }));
    } else {
      try {
        MastoAuth auth = new MastoAuth(server);
        bool avail = await auth.apiAvailable();
        if (avail) {
          oauth2.Client client = await auth.getClient();
          MastoApi api = new MastoApi(auth.serverUri, client);
          Navigator.of(context)
              .push(new MaterialPageRoute(builder: (BuildContext context) {
            return new WillPopScope(child: new RootPage(api), onWillPop: () async => false);
          }));
        } else {
          displayDialog('Error connecting',
              'Please check that the URL has been entered correctly and that the domain is not blocked by your ISP.');
        }
      } on CommError {

        displayDialog('Error connecting',
            'Please check that the URL has been entered correctly and that the domain is not blocked by your ISP.');
      } catch (e) {
        // display modal?
        print('Unkown exception: $e');
        displayDialog('Error', 'An unknown exception has occured.');
      }
    }
  }

  // initState runs only once unlike build (build reruns on push so it would just keep pushing forever)
  // hack? docs say that this is not the intended use case
  @override
  void initState() {
    _login();
    super.initState();
  }

  Future<void> displayDialog(String title, String message) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(child: Text(message)),
          actions: <Widget>[
            FlatButton(
              child: Text('Continue'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Center(child: new CircularProgressIndicator(value: null)));
  }
}
