# code layout notes

api/ - api stuff
api/auth.dart - spaghetti combo ui + api code (no way to separate this because of the custom tabs integration)

generated/ - i18n (unused)

api.models/ - api object api.models (originally taken from morii)

pages/ - views/general stuff

util/ - static utilities

widgets/ - ui components