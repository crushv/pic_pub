# pic_pub (release name undetermined)

A cross-platform client for the [PixelFed](https://pixelfed.org/) federated image sharing platform

# development notes

wip, nothing is final

## known issues

### android

### ios

* browser view does not close once oauth flow is complete
    * potential solution: scrap custom_tabs for own implem

## random

* switch ListView.builder to [StreamBuilder](https://blog.khophi.co/using-refreshindicator-with-flutter-streambuilder/)

## oauth

* uses fork: https://gitlab.com/crushv/oauth2.git
* first version done

## camera

not in immediate plans, stick with system camera for now

https://medium.com/@brianalois/flutter-how-to-use-pinch-zoom-for-the-camera-2018-d8a7c3399f9e

## storage

currently using shared_storage but that has potential for data loss on android (!!!)

potentially switch to [sqflite](https://pub.dartlang.org/packages/sqflite)

sqflite good idea because should be accessible from native sys as well (due to dart threads not being able to run in the background)

## apis

[Timeline `media_only`](https://github.com/tootsuite/documentation/blob/master/Using-the-API/API.md#timelines)

## external libs

* TODO: fork flutter_custom_tabs/make own because the current [underlying code](https://github.com/droibit/CustomTabsLauncher) doesn't allow anything besides chrome as the custom tabs provider (in the name of "user friendliness")
    * see known issues as well

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/
